# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is the 'room' controller for cornucopia application
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------


@auth.requires_login()
def create():
    """Creates a new container"""
    db.container.container_id.show_if = (db.container.nested == True)
    db.container.room_id.show_if = (db.container.nested == False)

    form = SQLFORM(db.container)

    if form.process().accepted:
        response.flash = 'form accepted'
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill out the form'
    return dict(form=form)


def show():
    """Shows a container"""

    try:
        request.args(0, cast=int)
    except:
        redirect(URL('default', 'index'))

    this_cont = db.container(request.args(0, cast=int))

    form = SQLFORM(db.container, this_cont).process() if auth.user else None

    this_cont_containers = None
    this_cont_containers = db(
        db.container.container_id == this_cont.id).select()

    this_cont_things = db(db.thing.container_id == this_cont.id).select()

    return dict(cont=this_cont,
                containers=this_cont_containers,
                things=this_cont_things,
                form=form)


def search():
    """an ajax wiki search page for cornucopia containers"""
    return dict(form=FORM(INPUT(_id='keyword',
                                _name='keyword',
                                _onkeyup="ajax('callback', ['keyword'], 'target');")),
                target_div=DIV(_id='target'))


def callback():
    """an ajax callback that returns a <ul> of links to cornucopia places"""
    query = db.container.name.contains(request.vars.keyword)
    objs = db(query).select(orderby=db.container.name)
    links = [A(ob.name, _href=URL('show', args=ob.id)) for ob in objs]
    return UL(*links)


def manage():
    """Manage container"""
    # grid = SQLFORM.grid(db.place)
    grid = SQLFORM.smartgrid(db.container)
    return dict(grid=grid)
