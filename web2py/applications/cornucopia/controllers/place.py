# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is the place controller for cornucopia application
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------


@auth.requires_login()
def create():
    """Creates a new place"""
    form = SQLFORM(db.place)

    if form.process().accepted:
        response.flash = 'form accepted'
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill out the form'
    return dict(form=form)


def show():
    """Shows a place"""

    try:
        request.args(0, cast=int)
    except:
        redirect(URL('default', 'index'))

    this_place = db.place(request.args(0, cast=int))
    db.room.place_id.default = this_place.id
    form = SQLFORM(db.place, this_place).process() if auth.user else None
    this_place_rooms = db(db.room.place_id == this_place.id).select(
        orderby=db.room.id)
    return dict(place=this_place,
                rooms=this_place_rooms,
                form=form)


def search():
    """an ajax wiki search page for cornucopia places"""
    return dict(form=FORM(INPUT(_id='keyword',
                                _name='keyword',
                                _onkeyup="ajax('callback', ['keyword'], 'target');")),
                target_div=DIV(_id='target'))


def callback():
    """an ajax callback that returns a <ul> of links to cornucopia places"""
    query = db.place.name.contains(request.vars.keyword)
    objs = db(query).select(orderby=db.place.name)
    links = [A(o.name, _href=URL('show', args=o.id)) for o in objs]
    return UL(*links)


@auth.requires_login()
def manage():
    """Manage Places"""
    # grid = SQLFORM.grid(db.place)
    grid = SQLFORM.smartgrid(db.place)
    # linked_tables = ['room'])
    return dict(grid=grid)


def test():
    grid = SQLFORM.grid(db.place)
    return dict(grid=grid)
